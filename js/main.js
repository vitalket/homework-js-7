/* 
   1. В мотод forEach ми передаємо колбек-функцію, яка виконується для кожного елемента,
      метод проходить по кожному елементу масиву, передаючи поточний елемент, 
      індекс та сам масив(індекс і масив не обов'язкові параметри) в якості аргументів у нашу колбек-функцію.

   2. Щоб очистити масив є декілька способів:
      - присвоїти порожній масив, arr = [];
      - через метод arr.slice(0);
      - присвоїти довжину масиву 0, arr.length = 0;

   3. Використання методу Array.isArray(myVar), поверне true або false
 */

let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, dataType) {
   const result = arr.filter(elem => typeof elem !== dataType)
   return result;
}

console.log(filterBy(arr, "string"));


















